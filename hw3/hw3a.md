
### Web scale data mining and processing
### Homework 3a
### Author: Maciej Szymkiewicz


    from __future__ import print_function
    import numpy as np
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import ImageGrid
    
    class GameOfLife(object):
        def __init__(self, nrow, ncol):
            """Create new game with empty grid nrow * ncol
            """
            self._grid = np.zeros((nrow + 2) * (ncol + 2), "i8").reshape(nrow + 2, ncol + 2)
            self._center = self._grid[1:-1, 1:-1]
    
        def set_fields(self, fields):
            """Retruns new game of life object with fields set to 1
            
            >>> gol = GameOfLife(5, 5).set_fields([1], [1])
            >>> gol.grid
            array([[0, 0, 0, 0, 0],
                   [0, 1, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0]])
            """
            new = GameOfLife(*self._center.shape)
            new._center[fields] = 1
            return new
        
        @property
        def grid(self):
            """Return current state
            """
            return self._center
            
        @property
        def neighbours_count(self):
            """ Count neighbours 
            
            >>> GameOfLife(3, 3).neighbours_count
            array([[0, 0, 0],
                   [0, 0, 0],
                   [0, 0, 0]])
            >>> GameOfLife(5, 5).set_fields([(1, 1), (1, 2), (2, 3)]).neighbours_count
            array([[1, 2, 2, 1, 0],
                   [1, 1, 2, 2, 1],
                   [1, 2, 3, 1, 1],
                   [0, 0, 1, 1, 1],
                   [0, 0, 0, 0, 0]])
            """
            return np.sum([
                self._grid[:-2, :-2],  # NW
                self._grid[:-2, 1:-1], # N
                self._grid[:-2, 2:],   # NE
        
                self._grid[1:-1, :-2], # W
                self._grid[1:-1, 2:],  # E
    
                self._grid[2:, :-2],   # SW
                self._grid[2:, 1:-1],  # S 
                self._grid[2:, 2:]     # SE
    
            ], 0)
        
        def plot(self):
            """ Plot grid
            """
            plt.imshow(self._center, interpolation='nearest', cmap=plt.cm.gray_r)
            
        def next(self):
            """ Create next generation
            
            >>> GameOfLife(3, 3).next().grid
            array([[0, 0, 0],
                   [0, 0, 0],
                   [0, 0, 0]])
            >>> GameOfLife(3, 3).set_fields([(1, 1)]).next().grid
            array([[0, 0, 0],
                   [0, 0, 0],
                   [0, 0, 0]])
            """
            counts = self.neighbours_count
            new = GameOfLife(*self._center.shape)
            new._center[(self._center == 0) & (counts == 3)] = 1
            new._center[(self._center == 1) & ((counts == 2) | (counts == 3))] = 1
            return new
            
        def randomize(self, p = 0.5):
            """ Return new random game
            
            >>> np.random.seed(1)
            >>> GameOfLife(3, 3).randomize().grid
            array([[1, 0, 1],
                   [1, 1, 1],
                   [1, 1, 1]])
            """
            new = GameOfLife(*self._center.shape)
            new._center[np.random.random(new._center.shape) < p] = 1
            return new
        
        def __unicode__(self):
            return "\n".join([
                " ".join([u"\u25A0" if x else u"\u25A1" for x in line])
                for line in self._center
            ])
        
        def __str__(self):
            return unicode(self).encode('utf-8')
        
        @staticmethod
        def glider(x = 0, y = 0, k = 0):
            glider = np.array([0, 1, 0, 0, 0, 1, 1, 1, 1]).reshape(3, 3)
            xs, ys = np.rot90(glider, k = k).nonzero()
            return xs + x, ys + y
        
        @staticmethod
        def evolve(game_of_life, niters, nrows_ncols = (4, 5), plot_dims = (20., 20.)):
            """ Run GOL for niters and plot results
            """
            interv = niters / (nrows_ncols[0] * nrows_ncols[1])
            
            fig = plt.figure(1, plot_dims)
            grid = ImageGrid(
                fig, 111,
                nrows_ncols = nrows_ncols,
                axes_pad = 0.1,
            )
    
            
            for i in range(niters):
                if i % interv == 0:
                    grid[i / interv].imshow(game_of_life.grid, interpolation='nearest', cmap=plt.cm.gray_r)
                game_of_life = game_of_life.next()
                    
        


    game_of_life = GameOfLife(30, 30).set_fields(
        tuple(np.concatenate((
            GameOfLife.glider(),
            GameOfLife.glider(5, 5, 1),
            GameOfLife.glider(20, 20, 2),
            GameOfLife.glider(8, 17, 3),
            GameOfLife.glider(22, 1, 1),
            GameOfLife.glider(0, 15),
            GameOfLife.glider(15, 25, 3)
        ), 1))
    )
    
    GameOfLife.evolve(game_of_life, 100)


![png](hw3a_files/hw3a_2_0.png)



    np.random.seed(42)
    game_of_life = GameOfLife(35, 35).randomize(p = 0.5)
    
    GameOfLife.evolve(game_of_life, 100)


![png](hw3a_files/hw3a_3_0.png)



    game_of_life = GameOfLife(35, 35).randomize(p = 0.25)
    
    GameOfLife.evolve(game_of_life, 100)


![png](hw3a_files/hw3a_4_0.png)



    np.random.seed(42)
    game_of_life = GameOfLife(35, 35).randomize(p = 0.75)
    
    GameOfLife.evolve(game_of_life, 100)


![png](hw3a_files/hw3a_5_0.png)



    
