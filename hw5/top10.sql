-- hadoop fs -mkdir hw51
-- hadoop fs -put smallNYC.csv hw51

CREATE TABLE violations (
    SummonNumber                  BIGINT, 
    PlateID                       STRING, 
    RegistrationState             STRING, 
    PlateType                     STRING, 
    IssueDate                     STRING, 
    ViolationCode                 BIGINT, 
    VehicleBodyType               STRING, 
    VehicleMake                   STRING, 
    IssuingAgency                 STRING, 
    StreetCode1                   BIGINT, 
    StreetCode2                   BIGINT, 
    StreetCode3                   BIGINT, 
    VehicleExpirationDate         BIGINT, 
    ViolationLocation             BIGINT, 
    ViolationPrecinct             BIGINT, 
    IssuerPrecinct                BIGINT, 
    IssuerCode                    BIGINT,
    IssuerCommand                 STRING, 
    IssuerSquad                   STRING, 
    ViolationTime                 STRING, 
    TimeFirstObserved             STRING, 
    ViolationCounty               STRING, 
    ViolationInFrontOfOrOpposite  STRING, 
    HouseNumber                   STRING, 
    StreetName                    STRING, 
    IntersectingStreet            STRING, 
    DateFirstObserved             BIGINT,  
    LawSection                    BIGINT, 
    SubDivision                   STRING,
    ViolationLegalCode            BOOLEAN,
    DaysParkingInEffect           STRING, 
    FromHoursInEffect             STRING, 
    ToHoursInEffect               STRING, 
    VehicleColor                  STRING, 
    UnregisteredVehicle           BIGINT, 
    VehicleYear                   BIGINT, 
    MeterNumber                   STRING,
    FeetFromCurb                  BIGINT, 
    ViolationPostCode             STRING, 
    ViolationDescription          STRING, 
    NoStandingorStoppingViolation BOOLEAN, 
    HydrantViolation              BOOLEAN, 
    DoubleParkingViolation        BOOLEAN  
 ) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

LOAD DATA INPATH 'hw51/smallNYC.csv' OVERWRITE INTO TABLE violations;

INSERT OVERWRITE DIRECTORY 'hw51/top10' SELECT CONCAT(StreetName, ' ', HouseNumber) FROM (
    SELECT StreetName, HouseNumber, COUNT(*) AS count FROM violations
    WHERE IssueDate RLIKE '201[2-4]$' AND HouseNumber RLIKE '^[0-9]+$'
    GROUP BY StreetName, HouseNumber
    ORDER BY count DESC
    LIMIT 10
) top10;

INSERT OVERWRITE DIRECTORY 'hw51/top10averages' SELECT CONCAT(StreetName, ' ', HouseNumber), count / 3.0 AS avergage FROM (
    SELECT StreetName, HouseNumber, COUNT(*) AS count FROM violations
    WHERE IssueDate RLIKE '201[2-4]$' AND HouseNumber RLIKE '^[0-9]+$'
    GROUP BY StreetName, HouseNumber
    ORDER BY count DESC
    LIMIT 10
) top10;


