#! /usr/bin/env python

import urllib
import logging
import json
import os


languages = (
    ("turkish", 1.1),
    ("swedish", 1.1),
    ("spanish", 1.1),
    ("russian", 1.1),
    ("portuguese", 1.1),
    ("norwegian", 1.1),
    ("italian", 1.1),
    ("hungarian", 1.1),
    ("german", 1.1),
    ("french", 1.1),
    ("finnish", 1.1),
    ("english", 1.2),
    ("dutch", 1.1),
    ("danish", 1.1)
)

base_url = (
    "http://anoncvs.postgresql.org/cvsweb.cgi/pgsql/"
    "src/backend/snowball/stopwords/{0}.stop?rev={1};content-type=text%2Fplain"
)


stopwords_json = os.path.join(os.path.dirname(os.path.realpath(__file__)), "stopwords.json")


def get_stopwords(lang, rev):
    """Download stopwords files for lang from PostgreSQL repostiory
    """
    url = base_url.format(lang, rev)
    try:
        f = urllib.urlopen(url)
        if f.code == 200:
            return [line.strip() for line in f]
        else:
            logging.warning("Failed to download {0} stopwords".format(lang))
    finally:
        f.close()



def main():
    if os.path.exists(stopwords_json):
        with open(stopwords_json) as fr:
            stopwords = json.load(fr)
    else:
        stopwords = dict(
                (lang, get_stopwords(lang, rev)) for lang, rev in languages
        )
        with open(stopwords_json, "w") as fw:
            json.dump(stopwords, fw)
    return dict((k, set(v)) for k, v in stopwords.items())


if __name__ == "__main__":
    main()
