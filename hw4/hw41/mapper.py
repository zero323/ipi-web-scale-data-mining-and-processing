#! /usr/bin/env python

import sys
import os
import json

with open("stopwords.json") as fr:
    stopwords = json.load(fr)


def count_stopwords(tokens, stopwords = stopwords):
    """Take list of tokens and dictionary {lang: [stopwords]}
    and return counts of stopwords for each language

    >>> tokens = "The quick brown fox jumps over the lazy dog".lower().split()
    >>> stopwords = {"english": {"the", "over"}}
    >>> counts = count_stopwords(tokens=tokens, stopwords=stopwords)
    >>> list(counts)
    [('english', 3)]
    """
    try:
        tokens = [token.decode('utf-8') for token in tokens]
    except UnicodeDecodeError:
        pass
    return ((lang, sum(token in v for token in tokens)) for lang, v in stopwords.items())

def mapper():
    """Predict input language based on stopwords abundance

    >>> import StringIO
    >>> sys.stdin = StringIO.StringIO("The quick brown fox jumps over the lazy dog")
    >>> mapper()
    ... # doctest: +NORMALIZE_WHITESPACE
    The quick brown fox jumps over the lazy dog english
    >>> sys.stdin = sys.__stdin__
    """
    for line in sys.stdin:
        tokens = line.lower().split()
        counts = count_stopwords(tokens)
        lang, count = max(counts, key=lambda x: x[1])
        print("{0}\t{1}\n".format(line.rstrip(), lang if count > 0 else "unknown"))
    
if __name__ == "__main__":
    mapper()
