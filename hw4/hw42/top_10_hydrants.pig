-- Load data set
raw = LOAD '$inputDirEx2/smallNYC.csv' USING PigStorage(',');

-- Extract columns of interest and cast to charrarrays
incidents = FOREACH raw GENERATE REGEX_EXTRACT((chararray)$4, '(19[0-9]{2}|20[0-1][0-9])$', 1) AS Year, (chararray)$24 AS StreetName, (chararray)$23 AS HouseNumber;

-- Filter by correct street number
incidents_clean = FILTER incidents BY HouseNumber MATCHES '^[0-9]+$';
incidents_addresses = FOREACH incidents_clean GENERATE CONCAT(CONCAT(StreetName, ' '), HouseNumber) as Address, Year; 

-- Group and count
incidents_groupped = GROUP incidents_addresses BY Address;
incidents_counts = FOREACH incidents_groupped GENERATE group AS Address, COUNT(incidents_addresses) AS count, incidents_addresses AS incidents;

-- Sort by count and take top10
incidents_ordered = ORDER incidents_counts by count DESC;
top10 = LIMIT incidents_ordered 10;

-- Store result
top10_addresses = FOREACH top10 GENERATE Address;
STORE top10_addresses INTO '$outputDirEx2/top10_addresses';

-- Flatten and filter by year
top10_flat = FOREACH top10 GENERATE FLATTEN(incidents);
top10_flat_2012_14 = FILTER top10_flat BY Year MATCHES '201[2-4]';

-- Aggregate and compute averages
top10_groupped_2012_14 = GROUP top10_flat_2012_14 BY Address;
top10_average_2012_14 = FOREACH top10_groupped_2012_14 GENERATE group AS Address, COUNT(top10_flat_2012_14) / 3.0 AS average;
top10_average_2012_14_ordered = ORDER top10_average_2012_14 BY average DESC;

-- Store results
STORE top10_average_2012_14_ordered INTO '$outputDirEx2/top10_average_2012_14';
