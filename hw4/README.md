# Homework 4

Author: Maciej Szymkiewicz


## Exercise 4.1

- `hw41/in/hc_sample` contains small sample taken from the [HC corpora](http://www.corpora.heliohost.org/).
- `hw41/stopwords.json` contains preprocessed stop words downloaded from the [PostgreSQL repository]( http://anoncvs.postgresql.org/cvsweb.cgi/pgsql/src/backend/snowball/stopwords/)
- stop words list has been prepared using `get_stopwords.py` script

### Example usage:

    hadoop fs -put hw41

    export HADOOP_HOME=/usr/lib/hadoop
    $HADOOP_HOME/bin/hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming.jar -input hw41/in -output hw41/out -mapper hw41/mapper.py -file hw41/mapper.py -file hw41/stopwords.json
    hadoop fs -cat hw41/out/part*


## Exercise 4.2

### Example usage:

    hadoop fs -put hw42

    pig -param inputDirEx2="hw42/in" -param outputDirEx2="hw42/out" hw42/top_10_hydrants.pig
    hadoop fs -cat hw42/out/top10_addresses/part-m-*
    hadoop fs -cat hw42/out/top10_average_2012_14/part-r-*
    hadoop fs -get hw42/out hw42/out


Output files can be found in the `hw4/out` directory.

## Exercise 4.3

### Example usage:

    hadoop fs -put hw43/workflow.xml
    oozie job -oozie http://localhost:11000/oozie -config hw43/conf.properties -run
