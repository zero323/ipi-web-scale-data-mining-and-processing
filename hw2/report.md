# Homework 2
Maciej Szymkiewicz  

## Exercise 2.1

- $S$ - singleton appears in the sample
- $SD$ - only one element from a pair appears in the sample
- $D$ - pair appears in the sample


$$
\begin{aligned}
P(S) = \frac{1}{100} \\
P(SD) = 2 \frac{1}{100} \frac{99}{100} = \frac{198}{10000} \\
P(D) = \frac{1}{100} \frac{1}{100} = \frac{1}{10000}\\
\end{aligned}
$$


$$
\begin{aligned}
\#(\mbox{singletons in the sample}) = \frac{s}{100} \\
\#(\mbox{single elements taken from pair}) =  \frac{198 d}{10000} \\
\#(\mbox{complete pairs}) =  \frac{d}{10000} \\
\end{aligned}
$$

Expected value of the fraction returned by the algorithm:
$$
\frac{\frac{d}{10000}}{\frac{d}{10000} + \frac{s}{100} +  \frac{198 d}{10000}} = \frac{d}{100 s + 199 d}
$$

## Exercise 2.2

Source code of the classifier is available on [Github](https://github.com/zero323/ipi-wsdmp-ex2/blob/master/R/hw2.R).

### 2.1 Preprocessing

Load required libraries


```r
set.seed(323)
library(caret)
library(ggplot2)
library(pander)
library(RCurl)
```

Download and install assignment package.


```r
devtools::install_github("zero323/ipi-wsdmp-ex2")
library(ipiWsdmpEx2)
```

Classifier has been tested using [Michigan Corpus of Upper-Level Student Papers](http://micase.elicorpora.info/). Corpus can be downloaded using provided  function.


```r
downloaded <- ipiWsdmpEx2::download_dataset()
```

Documents have been preprocessed using [XML](http://cran.r-project.org/web/packages/XML/index.html) and [tm](http://cran.r-project.org/web/packages/tm/index.html) packages to obtain document - term counts matrix.


```r
dtm <- ipiWsdmpEx2::get_counts(ipiWsdmpEx2::process_papers("./data/html/"))
```

Corpus metadata can be loaded using following command.


```r
url <- "https://raw.githubusercontent.com/zero323/ipi-wsdmp-ex2/master/data/micusp_papers.csv"
metadata <- read.csv(text = getURL(url))
```

### Corpus description


Corpus contains 829 documents assigned to 16 categories:
Biology, Civil & Environmental Engineering, Economics, Education, English, History & Classical Studies, Industrial & Operations Engineering, Linguistics, Mechanical Engineering, Natural Resources & Environment, Nursing, Philosophy, Physics, Political Science, Psychology, Sociology.

<img src="report_files/figure-html/unnamed-chunk-6-1.png" title="" alt="" style="display: block; margin: auto;" />

### Classifier

Documents have been assigned to train and test datasets using `createDataPartition` function from the the [caret](http://topepo.github.io/caret/index.html) package.


```r
train_index <- caret::createDataPartition(
    times = 1,
    metadata$DISCIPLINE,
    p = 0.7,
    list = FALSE
)

train_set <- dtm[train_index, ]
train_labels <- metadata$DISCIPLINE[train_index]
test_set  <- dtm[-train_index, ]
test_labels <- metadata$DISCIPLINE[-train_index]
```

Train set and test set contain 587 and 242 documents respectively.



```r
classifier <- ipiWsdmpEx2::train_naive(train_set, train_labels)
predicted <- predict_naive(classifier, test_set)
```


### Results


```r
confusion_matrix <- caret::confusionMatrix(predicted, test_labels)
```

#### Statistics


| &nbsp;                                       |  Sensitivity  |  Specificity  |  Prevalence  |  Detection Rate  |  Balanced Accuracy  |
|:---------------------------------------------|:-------------:|:-------------:|:------------:|:----------------:|:-------------------:|
| **1. Biology**                               |      0.9      |     0.991     |    0.083     |      0.074       |        0.945        |
| **2. Civil & Environmental Engineering**     |     0.556     |     0.996     |    0.037     |      0.021       |        0.776        |
| **3. Economics**                             |     0.429     |     0.991     |    0.029     |      0.012       |        0.71         |
| **4. Education**                             |     0.846     |     0.991     |    0.054     |      0.045       |        0.919        |
| **5. English**                               |     0.862     |     0.962     |     0.12     |      0.103       |        0.912        |
| **6. History & Classical Studies**           |     0.25      |     0.978     |     0.05     |      0.012       |        0.614        |
| **7. Industrial & Operations Engineering**   |     0.667     |     0.996     |     0.05     |      0.033       |        0.831        |
| **8. Linguistics**                           |     0.75      |     0.991     |     0.05     |      0.037       |        0.871        |
| **9. Mechanical Engineering**                |     0.889     |     0.987     |    0.037     |      0.033       |        0.938        |
| **10. Natural Resources & Environment**      |     0.833     |     0.978     |    0.074     |      0.062       |        0.906        |
| **11. Nursing**                              |     0.833     |     0.987     |     0.05     |      0.041       |        0.91         |
| **12. Philosophy**                           |     0.846     |     0.991     |    0.054     |      0.045       |        0.919        |
| **13. Physics**                              |       1       |       1       |    0.025     |      0.025       |          1          |
| **14. Political Science**                    |     0.778     |     0.969     |    0.074     |      0.058       |        0.873        |
| **15. Psychology**                           |     0.71      |     0.967     |    0.128     |      0.091       |        0.838        |
| **16. Sociology**                            |     0.762     |     0.964     |    0.087     |      0.066       |        0.863        |

#### Confusion matrix


| &nbsp;                                       |  1.  |  2.  |  3.  |  4.  |  5.  |  6.  |  7.  |  8.  |  9.  |  10.  |  11.  |  12.  |  13.  |  14.  |  15.  |  16.  |
|:---------------------------------------------|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| **1. Biology**                               |  18  |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |   1   |   0   |   0   |   0   |   0   |   1   |   0   |
| **2. Civil & Environmental Engineering**     |  0   |  5   |  0   |  0   |  0   |  0   |  0   |  0   |  1   |   0   |   0   |   0   |   0   |   0   |   0   |   0   |
| **3. Economics**                             |  0   |  0   |  3   |  0   |  0   |  2   |  0   |  0   |  0   |   0   |   0   |   0   |   0   |   0   |   0   |   0   |
| **4. Education**                             |  0   |  0   |  0   |  11  |  0   |  0   |  0   |  0   |  0   |   0   |   0   |   0   |   0   |   0   |   0   |   2   |
| **5. English**                               |  0   |  0   |  4   |  0   |  25  |  2   |  0   |  0   |  0   |   0   |   0   |   0   |   0   |   0   |   1   |   1   |
| **6. History & Classical Studies**           |  0   |  0   |  0   |  1   |  1   |  3   |  0   |  1   |  0   |   0   |   0   |   0   |   0   |   1   |   1   |   0   |
| **7. Industrial & Operations Engineering**   |  0   |  0   |  0   |  0   |  0   |  0   |  8   |  1   |  0   |   0   |   0   |   0   |   0   |   0   |   0   |   0   |
| **8. Linguistics**                           |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  9   |  0   |   0   |   0   |   1   |   0   |   0   |   1   |   0   |
| **9. Mechanical Engineering**                |  0   |  1   |  0   |  0   |  0   |  0   |  2   |  0   |  8   |   0   |   0   |   0   |   0   |   0   |   0   |   0   |
| **10. Natural Resources & Environment**      |  1   |  3   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  15   |   0   |   0   |   0   |   0   |   1   |   0   |
| **11. Nursing**                              |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |   0   |  10   |   0   |   0   |   0   |   2   |   1   |
| **12. Philosophy**                           |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |   0   |   0   |  11   |   0   |   0   |   2   |   0   |
| **13. Physics**                              |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |  0   |   0   |   0   |   0   |   6   |   0   |   0   |   0   |
| **14. Political Science**                    |  0   |  0   |  0   |  0   |  3   |  2   |  0   |  0   |  0   |   2   |   0   |   0   |   0   |  14   |   0   |   0   |
| **15. Psychology**                           |  1   |  0   |  0   |  1   |  0   |  0   |  1   |  1   |  0   |   0   |   2   |   0   |   0   |   0   |  22   |   1   |
| **16. Sociology**                            |  0   |  0   |  0   |  0   |  0   |  3   |  1   |  0   |  0   |   0   |   0   |   1   |   0   |   3   |   0   |  16   |


```r
remove.packages("ipiWsdmpEx2")
```

```
## Removing package from '/home/zero323/R/x86_64-pc-linux-gnu-library/3.1'
## (as 'lib' is unspecified)
```
