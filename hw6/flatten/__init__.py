from __future__ import print_function

def __process(fr):
    accum = {'gi': None, 'seq': ''}
    for line in fr:
        if line.strip().startswith('>'):
            if accum['seq']:
                yield '{0}\t{1}'.format(accum['gi'], accum['seq'])
            accum['gi'] = line.split('|')[1]
            accum['seq'] = ''
        else:
            accum['seq'] += line.strip()

    if accum['seq']:
        yield '{0}\t{1}'.format(accum['gi'], accum['seq'])


def flatten(fasta_path, flatten_path):
    with open(fasta_path) as fr, open(flatten_path, 'w') as fw:
        for rec in __process(fr):
            print(rec, file=fw)
