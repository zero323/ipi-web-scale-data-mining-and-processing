
# Web scale data mining and processing, Homework 6
Author: Maciej Szymkiewicz

## Task 1

```python
from flatten import flatten
import regex
from collections import namedtuple, defaultdict, Counter
from itertools import ifilter, islice
from itertools import chain
```

### Helper functions


```python
Sequence = namedtuple("Sequence", ("gi", "sequence"))

def get_ngrams(sequence, n):
    """ Given sequence and n return set of n-grams
    >>> sorted(get_ngrams("MKTKSSN", 3))
    ['KSS', 'KTK', 'MKT', 'SSN', 'TKS']
    """
    return {sequence[i:i + n] for i in range(len(sequence) - n + 1)}

def get_seeds(sequence, n):
    """ Get list of seeds
    >>> get_seeds("MKTKSSNNIKKIYY", 3)
    [(0, "MKT"), (3, "KSS"), (6, "NNI"), (9, "KKI"), (11, "IYY")]
    """
    l = len(sequence)
    assert(l >= n)
    seeds = [(i * n, sequence[i * n : (i + 1) * n]) for i in range(l / n)]
    return seeds + ([(l - n, sequence[l - n:])] if l % n else [])

def get_candidates(ngrams, inverse_index):
    """Given set of ngrams and inverse index
    return set of gis representing sequence
    containing at least one of the ngrams
    """
    return set(chain(*(inverse_index[ngram] for ngram in ngrams)))

def get_ordered_candidates(ngrams, inverse_index):
    """Given set of ngrams and inverse index
    return list of gis representing sequence
    ordered by number of common n-grams
    """
    counts = Counter(chain(*(inverse_index[ngram] for ngram in ngrams)))
    return [k for k, v in counts.most_common()]

def update_index(ngrams, inverse_index, gi):
    """Update inverse index
    """
    for ngram in ngrams:
        inverse_index[ngram].append(gi)
    return inverse_index

def check_candidates(sequence, gis, representatives):
    """ Use regex (https://pypi.python.org/pypi/regex) library
    to search for patterns
    """
    pattern = regex.compile("{0}{{s<={1}}}".format(sequence, int(0.1 * len(sequence))))
    for gi in gis:
        m = pattern.search(representatives[gi])
        if m is not None:
            return(gi)
    return None
```

### Clustering


```python
def cluster(
    input_file = "data/nr_flat",
    chunk_size = 100000,
    # If sequence is longer than 100
    # and we want to have at most 10% of mismatches
    # then at least one n-gram of length 9
    # has to be matched exactly 
    seed_size = 9,
    min_len = 200,
    max_len = 1000):

    # representative: list of members
    clusters = defaultdict(list)
    
    with open(input_file) as fr:
    
        # Filter sequences according to given criteria
        seqs = islice(
            ifilter(
                lambda _: min_len <= len(_.sequence) <= max_len,
                (Sequence(*line.split()) for line in fr)
            ), chunk_size)
        
        first = seqs.next()
        
        # representative: sequence
        representatives = {first.gi: first.sequence}
        clusters[first.gi].append(first.gi)
        
        # n-gram: list of gis
        inverse_index = update_index(
            get_ngrams(first.sequence, seed_size),
            defaultdict(list), first.gi)
        
        for seq in seqs:
            matched = None
        
            ngrams = get_ngrams(seq.sequence, seed_size)
            
            candidates = get_candidates(ngrams, inverse_index)
            
            cluster = check_candidates(seq.sequence, candidates, representatives)
            if cluster is not None:
                clusters[cluster].append(seq.gi)
            else:
                inverse_index = update_index(ngrams, inverse_index, seq.gi)
                representatives[seq.gi] = seq.sequence
                clusters[seq.gi].append(seq.gi)
                
    return clusters
```


### Remove metadata and flatten fasta file

```python
flatten("data/nr", "data/nr_flat")
```

### Run clustering

```python
%timeit cluster()

1 loops, best of 3: 2min 55s per loop
```

## Task 2

The biggest disadvantages of the provided method are:
- dependence on the ordering
- usage of a single sequence as representative of the cluster
- ignoring substantial differences in the sequence length
- not taking into account biological background (ignoring events like deletions
or insertions and amino acids substitutions)

Depending on a dataset it can lead to the multiple problems:
- almost identical sequences can be assigned to different clusters
- short sequences can be "consumed" by clusters with long representative
sequence purely by chance
- dependence on ordering limits our ability to parallelize computations

One possible approach to deal with above issues would be to start clustering
inside groups of sequences of a similar length. Instead of keeping only a single
sequence as a representative we can use profile based on multiple sequence
alignment of all of the members. After that we can iteratively merge groups
merging member clusters if needed.


## Appendix
### `flatten` module

```python
from __future__ import print_function

def __process(fr):
    accum = {'gi': None, 'seq': ''}
    for line in fr:
        if line.strip().startswith('>'):
            if accum['seq']:
                yield '{0}\t{1}'.format(accum['gi'], accum['seq'])
            accum['gi'] = line.split('|')[1]
            accum['seq'] = ''
        else:
            accum['seq'] += line.strip()

    if accum['seq']:
        yield '{0}\t{1}'.format(accum['gi'], accum['seq'])


def flatten(fasta_path, flatten_path):
    with open(fasta_path) as fr, open(flatten_path, 'w') as fw:
        for rec in __process(fr):
            print(rec, file=fw)
```
