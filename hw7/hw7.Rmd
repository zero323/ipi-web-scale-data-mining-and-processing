
```{r}
library(stringi)
library(data.table)
library(XML)
library(cluster)
```

```{r}
local({
    base_url <- 'https://archive.org/download/stackexchange'
    files <- list(
        list(main = 'biology.stackexchange.com.7z'),
        list(main = 'datascience.stackexchange.com.7z'),
        list(main = 'homebrew.stackexchange.com.7z'),
        list(main = 'robotics.stackexchange.com.7z'),
        list(main = 'opendata.stackexchange.com.7z')
    )
    
    # Extract question id, score, and tags from question
    parse_questions <- function(lines, alltags) {
        parsed <- stri_match_all_regex(
            Filter(function(l) grepl('^\\s*<row.*Tags', l), lines),
            ' Id="([0-9]*)" .* Score="([0-9]*)" .* Tags="([^"]*)"'
        )
        
        do.call(rbind, lapply(
            parsed,
            function(x) {
                c(
                    as.integer(x[2:3]),
                    alltags %in% stri_split_regex(x[4],  '&lt;|&gt;', omit_empty = TRUE)
                )
            }
        ))
    }
    
    # Extract question id and answer author from answer
    parse_answers <- function(lines) {
        parsed <- stri_match_all_regex(
            Filter(function(l) grepl('^\\s*<row.*ParentId', l), lines),
            ' ParentId="([0-9]*)" .* OwnerUserId="([0-9]*)"'
        )
        
        do.call(rbind, parsed)[, 2:3]
    }
    
    
    for(f in files) {
        f <- f$main
        
        destfile <- paste('data', f, sep = '/')
        
        # Download file
        if(!file.exists(destfile)) {
            download.file(
                url = paste(base_url, f, sep = '/'),
                destfile = destfile,
                method = 'wget'
            )
        }
        
        # Prepare file names
        xml_name <- paste('data/', gsub('(-Posts)?\\.7z', '', f), sep = '')
        tsv_name <- paste(xml_name, 'tsv', sep = '.')
        tags_name <- paste(xml_name, 'tags', sep = '.')
        answers_name <- paste(xml_name, 'answers.tsv', sep = '.')
        
        # Extract archives
        if(!file.exists(xml_name) & !file.exists(tsv_name)) {
            system(command = paste('7z e', destfile,  '-odata'))
            file.rename('data/Posts.xml', xml_name)
            file.rename('data/Tags.xml', tags_name)
            unlink('data/*xml')
        }
        
        # Create tag matrices
        if(!file.exists(tsv_name)) {
            
            # Extract all tags
            counts <- as.integer(xpathSApply(xmlParse(tags_name), '//row[@TagName]', xmlGetAttr, 'Count'))
            tags <- sort(xpathSApply(xmlParse(tags_name), '//row[@TagName]', xmlGetAttr, 'TagName')[counts > 1])
            
            
            
            # Since I cannot parse large XML due to limited memory
            # I'll process files manualy line by line
            in_con <- file(description = xml_name, open = 'r')
            out_con <- file(description = tsv_name, open = 'w')
            out_answers_con <- file(description = answers_name, open = 'w')
            
            writeLines(text = paste(c('id', 'score', tags), collapse = '\t'), out_con)
            writeLines(text = paste(c('qid', 'uid'), collapse = '\t'), out_answers_con)
            
            repeat {
                lines <- readLines(in_con, 1000, warn = FALSE)
                if (length(lines) == 0) break
                questions <- parse_questions(lines, tags)
                write.table(
                    questions, file = out_con,
                    row.names = FALSE, col.names = FALSE,
                    sep = "\t", quote = FALSE
                ) 
                
                answers <- parse_answers(lines)
                write.table(
                    answers, file = out_answers_con,
                    row.names = FALSE, col.names = FALSE,
                    sep = '\t', quote = FALSE
                ) 
            }
            close(in_con)
            close(out_con)
            close(out_answers_con)
            unlink('data/*com')
        }
    }
})
```

## biology.stackexchange.com

### Tags

```{r biology_tags, cache=TRUE}
biology_tags <- fread('data/biology.stackexchange.com.tsv')
biology_dists <- dist(t(biology_tags[, -c(1, 2), with=FALSE]), method="binary")
```

```{r biology_dendrogram, fig.width=12}
plot(agnes(biology_dists),  which=2)
```

### Recommendations

## datascience.stackexchange.com

```{r datascience_tags, cache=TRUE}
datascience_tags <- fread('data/datascience.stackexchange.com.tsv')
datascience_dists <- dist(t(datascience_tags[, -c(1, 2), with=FALSE]), method="binary")
```

```{r datascience_dendrogram, fig.width=12}
plot(agnes(datascience_dists),  which=2)
```

## homebrew.stackexchange.com

```{r homebrew_tags, cache=TRUE}
homebrew_tags <- fread('data/homebrew.stackexchange.com.tsv')
homebrew_dists <- dist(t(homebrew_tags[, -c(1, 2), with=FALSE]), method="binary")
```

```{r homebrew_dendrogram, fig.width=12}
plot(agnes(homebrew_dists),  which=2)
```

## robotics.stackexchange.com


```{r robotics_tags, cache=TRUE}
robotics_tags <- fread('data/robotics.stackexchange.com.tsv')
robotics_dists <- dist(t(robotics_tags[, -c(1, 2), with=FALSE]), method="binary")
```

```{r robotics_dendrogram, fig.width=12}
plot(agnes(robotics_dists),  which=2)
```

## opendata.stackexchange.com

```{r opendata_tags, cache=TRUE}
opendata_tags <- fread('data/opendata.stackexchange.com.tsv')
opendata_dists <- dist(t(opendata_tags[, -c(1, 2), with=FALSE]), method="binary")
```

```{r opendata_dendrogram, fig.width=12}
plot(agnes(opendata_dists),  which=2)
```

```{r}
function(tags) {
    get_score <- function(x, y) {
        tags_x <- tags[, x, with = FALSE]
        tags_y <- tags[, y, with = FALSE]
        list(sum(tags_x & tags_y), sum(tags_x))
    }
    
    pairs_dt <- as.data.table(expand.grid(colnames(tags), colnames(tags)))
    
    apply(pairs_dt, 1, function(x) get_score(x[1], x[2]))
    
}
```