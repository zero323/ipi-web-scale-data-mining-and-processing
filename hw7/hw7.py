
# coding: utf-8

# ## Author: Maciej Szymkiewicz
# 
# Following datasets have been used:
# 
# - biology.stackexchange.com
# - datascience.stackexchange.com
# - homebrew.stackexchange.com
# - opendata.stackexchange.com
# - robotics.stackexchange.com
# 
# These datasets are small and relatively sparse and some results can be misleading. It is particularly obvious when we look at recommender output.
# 
# Datasets have been downloaded from https://archive.org/details/stackexchange and extracted using 7-Zip 9.20.
# 
# 

# In[1]:

from __future__ import division, print_function
import lxml
import igraph
import pandas as pd
import re
import os
import itertools
from scipy.spatial.distance import pdist, cdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram
import rpy2.robjects as robjects
import pandas.rpy.common as com
from IPython.display import Image

pylab.rcParams['figure.figsize'] = (17.0, 8.0)


# In[2]:

def f_asymetric(x, y):
    return sum(x & y) / sum(x)


def get_edges(questions, threshold, min_tagged=5):
    questions = questions.loc[:, questions.sum(0) > 5]
    tags = list(questions.columns.values)
    pairs = ((t1, t2) for t1,  t2 in itertools.product(tags, tags) if t1 != t2)
    dists = ((t1, t2, f_asymetric(questions[t1], questions[t2]))  for t1, t2 in pairs)
    return [x for x in dists if x[2] >= threshold]


def create_graph(edges):
    g = igraph.Graph(directed = True)
    edges = [(x, y) for x, y, d in edges]
    vertices = list({_ for _ in itertools.chain(*edges)})
    g.add_vertices(vertices)
    g.vs['label'] = vertices
    g.add_edges(edges)
    return g


def parse_answers(input_file):
    def to_dict(path):
        return {'_uid_': x.xpath('@OwnerUserId')[0], x.xpath('@ParentId')[0]: 1}

    root = lxml.etree.parse(input_file)
    answers = root.xpath("/posts/row[@PostTypeId=2 and @OwnerUserId and @ParentId]")
    parsed = [to_dict(x) for x in answers]
    unique_parents = list({x.xpath('@ParentId')[0] for x in  answers})

    return pd.DataFrame(parsed, columns=['_uid_'] + unique_parents).fillna(0)


def parse_questions(input_file, all_tags):
    def to_dict(path, all_tags):
        qid = int(x.xpath('@Id')[0])
        score = int(x.xpath('@Score')[0])
        tags = {_ for _ in re.split('<|>', x.xpath("@Tags")[0]) if _}
        return dict(zip(
            ['_qid_', '_score_'] + all_tags,
            [qid, score] + [tag in tags for tag in all_tags]
        ))

    root = lxml.etree.parse(input_file)
    questions = [to_dict(x, all_tags) for x in root.xpath("/posts/row[@PostTypeId=1]")]
    return pd.DataFrame(questions, columns = ['_qid_', '_score_'] + all_tags)


def parse_tags(input_file):
    root = lxml.etree.parse(input_file)
    return root.xpath('/tags/row/@TagName')


def recommend(df, min_obs=5, min_given=1):
    df.to_csv('temp.csv', index=False)
    # Code based on:
    # Bolikowski, Dendek, Kursa, Web-scale data mining and processing
    # Big data in R with StackExchange case study
    recommend = robjects.r('''
        library(recommenderlab)
        recommend <- function(f, min_obs=1, given=1) {
            m <- as.matrix(read.csv(f))
            brm <- as(m[, colSums(m) >= min_obs], 'binaryRatingMatrix')
            image_file <- paste(tempfile(), 'png', sep='.')

            algos<-list(
                baseline=list(name='RANDOM', param=NULL),
                JaccardCF_5=list(name='UBCF',  param=list(method='Jaccard', nn=5)),
                JaccardCF_10=list(name='UBCF', param=list(method='Jaccard', nn=10)),
                JaccardCF_30=list(name='UBCF', param=list(method='Jaccard', nn=30)),
                JaccardCF_50=list(name='UBCF', param=list(method='Jaccard', nn=50))
            )
            es <- evaluationScheme(brm, 'cross-validation', k=10, given=given)
            results <- evaluate(es, algos, n=c(1, 3, 5, 10, 30))

            tryCatch({
                png(image_file, width = 960, height = 960)
                plot(results)
                dev.off()
                image_file
            }, error = function(e)  "")
        }''')
    return recommend('temp.csv', min_obs, min_given)


def process(directory, threshold=0.75, min_tagged=5):
    tags = parse_tags(os.path.join(directory, 'Tags.xml'))
    questions = parse_questions(os.path.join(directory, 'Posts.xml'), tags)
    answers = parse_answers(os.path.join(directory, 'Posts.xml'))
    dm = pdist(questions.iloc[:, 3:].as_matrix().transpose(), 'jaccard')
    edges = get_edges(questions.iloc[:, 3:], threshold, min_tagged)
    return tags, questions, dm, edges, answers


# ### biology

# In[3]:

tags, questions, dm, edges, answers = process('data/biology/')
dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


# In[4]:

g = create_graph(edges)
igraph.plot(g, layout = g.layout("kk"))


# In[5]:

f = recommend(answers, min_obs=4)[0]
Image(f)


# ### datascience

# In[6]:

tags, questions, dm, edges, answers = process('data/datascience/', min_tagged = 3)
dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


# In[7]:

g = create_graph(edges)
igraph.plot(g, layout = g.layout("kk"))


# In[8]:

f = recommend(answers, min_obs=5)[0]
Image(f)


# ### homebrew

# In[9]:

tags, questions, dm, edges, answers = process('data/homebrew/')
dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


# In[10]:

f = recommend(answers, min_obs=5)[0]
Image(f)


# ### opendata

# In[11]:

tags, questions, dm, edges, answers = process('data/opendata/')
dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


# In[12]:

g = create_graph(edges)
igraph.plot(g, layout = g.layout("kk"))


# In[13]:

g = create_graph(edges)
igraph.plot(g, layout = g.layout("kk"))


# ### robotics

# In[14]:

tags, questions, dm, edges, answers = process('data/robotics/', min_tagged = 1)
dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


# In[15]:

f = recommend(answers, min_obs=3)[0]
Image(f)

