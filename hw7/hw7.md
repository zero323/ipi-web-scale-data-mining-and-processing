
## Author: Maciej Szymkiewicz

Following datasets have been used:

- biology.stackexchange.com
- datascience.stackexchange.com
- homebrew.stackexchange.com
- opendata.stackexchange.com
- robotics.stackexchange.com

These datasets are small and relatively sparse and some results can be
misleading. It is particularly obvious when we look at recommender output.

Datasets have been downloaded from https://archive.org/details/stackexchange and
extracted using 7-Zip 9.20.




    from __future__ import division, print_function
    import lxml
    import igraph
    import pandas as pd
    import re
    import os
    import itertools
    from scipy.spatial.distance import pdist, cdist, squareform
    from scipy.cluster.hierarchy import linkage, dendrogram
    import rpy2.robjects as robjects
    import pandas.rpy.common as com
    from IPython.display import Image
    
    pylab.rcParams['figure.figsize'] = (17.0, 8.0)


    def f_asymetric(x, y):
        return sum(x & y) / sum(x)
    
    
    def get_edges(questions, threshold, min_tagged=5):
        questions = questions.loc[:, questions.sum(0) > 5]
        tags = list(questions.columns.values)
        pairs = ((t1, t2) for t1,  t2 in itertools.product(tags, tags) if t1 != t2)
        dists = ((t1, t2, f_asymetric(questions[t1], questions[t2]))  for t1, t2 in pairs)
        return [x for x in dists if x[2] >= threshold]
    
    
    def create_graph(edges):
        g = igraph.Graph(directed = True)
        edges = [(x, y) for x, y, d in edges]
        vertices = list({_ for _ in itertools.chain(*edges)})
        g.add_vertices(vertices)
        g.vs['label'] = vertices
        g.add_edges(edges)
        return g
    
    
    def parse_answers(input_file):
        def to_dict(path):
            return {'_uid_': x.xpath('@OwnerUserId')[0], x.xpath('@ParentId')[0]: 1}
    
        root = lxml.etree.parse(input_file)
        answers = root.xpath("/posts/row[@PostTypeId=2 and @OwnerUserId and @ParentId]")
        parsed = [to_dict(x) for x in answers]
        unique_parents = list({x.xpath('@ParentId')[0] for x in  answers})
    
        return pd.DataFrame(parsed, columns=['_uid_'] + unique_parents).fillna(0)
    
    
    def parse_questions(input_file, all_tags):
        def to_dict(path, all_tags):
            qid = int(x.xpath('@Id')[0])
            score = int(x.xpath('@Score')[0])
            tags = {_ for _ in re.split('<|>', x.xpath("@Tags")[0]) if _}
            return dict(zip(
                ['_qid_', '_score_'] + all_tags,
                [qid, score] + [tag in tags for tag in all_tags]
            ))
    
        root = lxml.etree.parse(input_file)
        questions = [to_dict(x, all_tags) for x in root.xpath("/posts/row[@PostTypeId=1]")]
        return pd.DataFrame(questions, columns = ['_qid_', '_score_'] + all_tags)
    
    
    def parse_tags(input_file):
        root = lxml.etree.parse(input_file)
        return root.xpath('/tags/row/@TagName')
    
    
    def recommend(df, min_obs=5, min_given=1):
        df.to_csv('temp.csv', index=False)
        # Code based on:
        # Bolikowski, Dendek, Kursa, Web-scale data mining and processing
        # Big data in R with StackExchange case study
        recommend = robjects.r('''
            library(recommenderlab)
            recommend <- function(f, min_obs=1, given=1) {
                m <- as.matrix(read.csv(f))
                brm <- as(m[, colSums(m) >= min_obs], 'binaryRatingMatrix')
                image_file <- paste(tempfile(), 'png', sep='.')
    
                algos<-list(
                    baseline=list(name='RANDOM', param=NULL),
                    JaccardCF_5=list(name='UBCF',  param=list(method='Jaccard', nn=5)),
                    JaccardCF_10=list(name='UBCF', param=list(method='Jaccard', nn=10)),
                    JaccardCF_30=list(name='UBCF', param=list(method='Jaccard', nn=30)),
                    JaccardCF_50=list(name='UBCF', param=list(method='Jaccard', nn=50))
                )
                es <- evaluationScheme(brm, 'cross-validation', k=10, given=given)
                results <- evaluate(es, algos, n=c(1, 3, 5, 10, 30))
    
                tryCatch({
                    png(image_file, width = 960, height = 960)
                    plot(results)
                    dev.off()
                    image_file
                }, error = function(e)  "")
            }''')
        return recommend('temp.csv', min_obs, min_given)
    
    
    def process(directory, threshold=0.75, min_tagged=5):
        tags = parse_tags(os.path.join(directory, 'Tags.xml'))
        questions = parse_questions(os.path.join(directory, 'Posts.xml'), tags)
        answers = parse_answers(os.path.join(directory, 'Posts.xml'))
        dm = pdist(questions.iloc[:, 3:].as_matrix().transpose(), 'jaccard')
        edges = get_edges(questions.iloc[:, 3:], threshold, min_tagged)
        return tags, questions, dm, edges, answers

### biology


    tags, questions, dm, edges, answers = process('data/biology/')
    dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


![png](hw7_files/hw7_4_0.png)



    g = create_graph(edges)
    igraph.plot(g, layout = g.layout("kk"))




![svg](hw7_files/hw7_5_0.svg)




    f = recommend(answers, min_obs=4)[0]
    Image(f)

    Loading required package: Matrix
    Loading required package: registry
    Loading required package: arules
    
    Attaching package: ‘arules’
    
    The following objects are masked from ‘package:base’:
    
        %in%, write
    
    Loading required package: proxy
    
    Attaching package: ‘proxy’
    
    The following objects are masked from ‘package:stats’:
    
        as.dist, dist
    
    RANDOM run 
    	 1  [0.008sec/0.564sec] 
    	 2  [0.004sec/0.536sec] 
    	 3  [0sec/0.54sec] 
    	 4  [0sec/0.524sec] 
    	 5  [0.004sec/0.516sec] 
    	 6  [0sec/0.516sec] 
    	 7  [0.004sec/0.516sec] 
    	 8  [0sec/0.516sec] 
    	 9  [0.004sec/0.52sec] 
    	 10  [0.004sec/0.52sec] 
    UBCF run 
    	 1  [0.004sec/7.54sec] 
    	 2  [0sec/7.66sec] 
    	 3  [0sec/7.38sec] 
    	 4  [0sec/7.34sec] 
    	 5  [0.004sec/7.284sec] 
    	 6  [0sec/7.288sec] 
    	 7  [0.004sec/7.164sec] 
    	 8  [0sec/7.308sec] 
    	 9  [0sec/7.288sec] 
    	 10  [0sec/7.184sec] 
    UBCF run 
    	 1  [0.004sec/7.34sec] 
    	 2  [0.004sec/7.516sec] 
    	 3  [0.004sec/7.712sec] 
    	 4  [0sec/7.644sec] 
    	 5  [0sec/7.392sec] 
    	 6  [0sec/7.412sec] 
    	 7  [0sec/7.968sec] 
    	 8  [0sec/7.456sec] 
    	 9  [0.004sec/7.32sec] 
    	 10  [0sec/7.5sec] 
    UBCF run 
    	 1  [0sec/7.66sec] 
    	 2  [0.004sec/7.28sec] 
    	 3  [0sec/7.512sec] 
    	 4  [0sec/7.464sec] 
    	 5  [0sec/7.512sec] 
    	 6  [0sec/7.36sec] 
    	 7  [0.004sec/7.456sec] 
    	 8  [0sec/7.572sec] 
    	 9  [0sec/7.86sec] 
    	 10  [0.004sec/7.976sec] 
    UBCF run 
    	 1  [0.004sec/8.104sec] 
    	 2  [0sec/7.452sec] 
    	 3  [0.004sec/7.416sec] 
    	 4  [0.004sec/7.62sec] 
    	 5  [0.004sec/7.584sec] 
    	 6  [0sec/7.528sec] 
    	 7  [0.004sec/7.544sec] 
    	 8  [0sec/7.264sec] 
    	 9  [0sec/7.536sec] 
    	 10  [0sec/7.656sec] 





![png](hw7_files/hw7_6_1.png)



### datascience


    tags, questions, dm, edges, answers = process('data/datascience/', min_tagged = 3)
    dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


![png](hw7_files/hw7_8_0.png)



    g = create_graph(edges)
    igraph.plot(g, layout = g.layout("kk"))




![svg](hw7_files/hw7_9_0.svg)




    f = recommend(answers, min_obs=5)[0]
    Image(f)

    RANDOM run 
    	 1  [0.004sec/0.028sec] 
    	 2  [0sec/0.024sec] 
    	 3  [0.004sec/0.028sec] 
    	 4  [0sec/0.028sec] 
    	 5  [0sec/0.032sec] 
    	 6  [0.004sec/0.028sec] 
    	 7  [0sec/0.028sec] 
    	 8  [0.004sec/0.028sec] 
    	 9  [0.004sec/0.032sec] 
    	 10  [0.004sec/0.024sec] 
    UBCF run 
    	 1  [0.004sec/0.508sec] 
    	 2  [0.004sec/0.464sec] 
    	 3  [0sec/0.504sec] 
    	 4  [0sec/0.488sec] 
    	 5  [0sec/0.492sec] 
    	 6  [0.004sec/0.496sec] 
    	 7  [0sec/0.488sec] 
    	 8  [0sec/0.504sec] 
    	 9  [0sec/0.52sec] 
    	 10  [0sec/0.488sec] 
    UBCF run 
    	 1  [0sec/0.492sec] 
    	 2  [0sec/0.504sec] 
    	 3  [0sec/0.504sec] 
    	 4  [0.004sec/0.5sec] 
    	 5  [0sec/0.484sec] 
    	 6  [0sec/0.48sec] 
    	 7  [0.004sec/0.5sec] 
    	 8  [0sec/0.516sec] 
    	 9  [0sec/0.512sec] 
    	 10  [0sec/0.468sec] 
    UBCF run 
    	 1  [0.004sec/0.464sec] 
    	 2  [0sec/0.488sec] 
    	 3  [0.004sec/0.504sec] 
    	 4  [0sec/0.476sec] 
    	 5  [0sec/0.484sec] 
    	 6  [0sec/0.5sec] 
    	 7  [0.004sec/0.504sec] 
    	 8  [0.004sec/0.484sec] 
    	 9  [0sec/0.504sec] 
    	 10  [0sec/0.5sec] 
    UBCF run 
    	 1  [0sec/0.576sec] 
    	 2  [0sec/0.496sec] 
    	 3  [0.004sec/0.504sec] 
    	 4  [0sec/0.484sec] 
    	 5  [0sec/0.492sec] 
    	 6  [0sec/0.492sec] 
    	 7  [0sec/0.468sec] 
    	 8  [0.004sec/0.468sec] 
    	 9  [0.004sec/0.468sec] 
    	 10  [0sec/0.468sec] 





![png](hw7_files/hw7_10_1.png)



### homebrew


    tags, questions, dm, edges, answers = process('data/homebrew/')
    dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


![png](hw7_files/hw7_12_0.png)



    f = recommend(answers, min_obs=5)[0]
    Image(f)

    RANDOM run 
    	 1  [0.004sec/0.752sec] 
    	 2  [0sec/0.956sec] 
    	 3  [0sec/0.804sec] 
    	 4  [0.004sec/0.776sec] 
    	 5  [0sec/0.768sec] 
    	 6  [0sec/0.752sec] 
    	 7  [0sec/0.74sec] 
    	 8  [0sec/0.76sec] 
    	 9  [0sec/0.744sec] 
    	 10  [0sec/0.884sec] 
    UBCF run 
    	 1  [0sec/8.9sec] 
    	 2  [0.004sec/8.388sec] 
    	 3  [0.004sec/8.252sec] 
    	 4  [0sec/7.992sec] 
    	 5  [0sec/7.944sec] 
    	 6  [0sec/7.876sec] 
    	 7  [0sec/7.944sec] 
    	 8  [0.004sec/7.976sec] 
    	 9  [0sec/8.312sec] 
    	 10  [0sec/7.972sec] 
    UBCF run 
    	 1  [0sec/8.136sec] 
    	 2  [0sec/8.236sec] 
    	 3  [0sec/8.312sec] 
    	 4  [0.004sec/8.048sec] 
    	 5  [0.004sec/8.34sec] 
    	 6  [0.004sec/8.096sec] 
    	 7  [0.004sec/8.144sec] 
    	 8  [0sec/8sec] 
    	 9  [0.004sec/8.084sec] 
    	 10  [0.004sec/7.908sec] 
    UBCF run 
    	 1  [0sec/8sec] 
    	 2  [0sec/7.976sec] 
    	 3  [0sec/7.984sec] 
    	 4  [0sec/8.036sec] 
    	 5  [0.004sec/7.984sec] 
    	 6  [0.004sec/7.988sec] 
    	 7  [0sec/7.964sec] 
    	 8  [0sec/8.024sec] 
    	 9  [0.004sec/8.024sec] 
    	 10  [0sec/7.988sec] 
    UBCF run 
    	 1  [0.004sec/7.988sec] 
    	 2  [0.004sec/8.004sec] 
    	 3  [0sec/7.98sec] 
    	 4  [0sec/7.952sec] 
    	 5  [0sec/8.06sec] 
    	 6  [0sec/7.992sec] 
    	 7  [0sec/7.98sec] 
    	 8  [0sec/7.888sec] 
    	 9  [0sec/8.052sec] 
    	 10  [0sec/8.168sec] 





![png](hw7_files/hw7_13_1.png)



### opendata


    tags, questions, dm, edges, answers = process('data/opendata/')
    dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


![png](hw7_files/hw7_15_0.png)



    g = create_graph(edges)
    igraph.plot(g, layout = g.layout("kk"))




![svg](hw7_files/hw7_16_0.svg)




    f = recommend(answers, min_obs=5)[0]
    Image(f)

    RANDOM run 
    	 1  [0sec/0.084sec] 
    	 2  [0sec/0.08sec] 
    	 3  [0.004sec/0.08sec] 
    	 4  [0.004sec/0.08sec] 
    	 5  [0.004sec/0.08sec] 
    	 6  [0.004sec/0.08sec] 
    	 7  [0.004sec/0.08sec] 
    	 8  [0sec/0.08sec] 
    	 9  [0.004sec/0.08sec] 
    	 10  [0.004sec/0.084sec] 
    UBCF run 
    	 1  [0.004sec/1.216sec] 
    	 2  [0sec/1.204sec] 
    	 3  [0sec/1.232sec] 
    	 4  [0sec/1.236sec] 
    	 5  [0sec/1.212sec] 
    	 6  [0sec/1.2sec] 
    	 7  [0sec/1.236sec] 
    	 8  [0sec/1.196sec] 
    	 9  [0.004sec/1.34sec] 
    	 10  [0.004sec/1.196sec] 
    UBCF run 
    	 1  [0sec/1.22sec] 
    	 2  [0sec/1.276sec] 
    	 3  [0.004sec/1.268sec] 
    	 4  [0.004sec/1.204sec] 
    	 5  [0sec/1.192sec] 
    	 6  [0.004sec/1.212sec] 
    	 7  [0sec/1.224sec] 
    	 8  [0sec/1.228sec] 
    	 9  [0sec/1.216sec] 
    	 10  [0.004sec/1.32sec] 
    UBCF run 
    	 1  [0sec/1.212sec] 
    	 2  [0sec/1.216sec] 
    	 3  [0sec/1.292sec] 
    	 4  [0sec/1.248sec] 
    	 5  [0.004sec/1.232sec] 
    	 6  [0sec/1.364sec] 
    	 7  [0sec/1.248sec] 
    	 8  [0.004sec/1.188sec] 
    	 9  [0.004sec/1.196sec] 
    	 10  [0sec/1.196sec] 
    UBCF run 
    	 1  [0sec/1.304sec] 
    	 2  [0sec/1.196sec] 
    	 3  [0sec/1.22sec] 
    	 4  [0sec/1.232sec] 
    	 5  [0.004sec/1.24sec] 
    	 6  [0sec/1.204sec] 
    	 7  [0sec/1.212sec] 
    	 8  [0.004sec/1.232sec] 
    	 9  [0sec/1.244sec] 
    	 10  [0sec/1.196sec] 





![png](hw7_files/hw7_17_1.png)



### robotics


    tags, questions, dm, edges, answers = process('data/robotics/', min_tagged = 1)
    dendr = dendrogram(linkage(dm, metric='jaccard'), labels = tags, no_labels=True)


![png](hw7_files/hw7_19_0.png)



    f = recommend(answers, min_obs=3)[0]
    Image(f)

    RANDOM run 
    	 1  [0.004sec/0.172sec] 
    	 2  [0sec/0.172sec] 
    	 3  [0.004sec/0.164sec] 
    	 4  [0.004sec/0.172sec] 
    	 5  [0.004sec/0.176sec] 
    	 6  [0sec/0.172sec] 
    	 7  [0.004sec/0.172sec] 
    	 8  [0.004sec/0.172sec] 
    	 9  [0sec/0.168sec] 
    	 10  [0.004sec/0.164sec] 
    UBCF run 
    	 1  [0.004sec/1.6sec] 
    	 2  [0sec/1.612sec] 
    	 3  [0.004sec/1.64sec] 
    	 4  [0sec/1.572sec] 
    	 5  [0sec/1.64sec] 
    	 6  [0sec/1.64sec] 
    	 7  [0sec/1.532sec] 
    	 8  [0sec/1.632sec] 
    	 9  [0.004sec/1.548sec] 
    	 10  [0sec/1.596sec] 
    UBCF run 
    	 1  [0.004sec/1.652sec] 
    	 2  [0.004sec/1.568sec] 
    	 3  [0.004sec/1.58sec] 
    	 4  [0sec/1.548sec] 
    	 5  [0sec/1.6sec] 
    	 6  [0.004sec/1.528sec] 
    	 7  [0sec/1.632sec] 
    	 8  [0sec/1.584sec] 
    	 9  [0.004sec/1.604sec] 
    	 10  [0sec/1.568sec] 
    UBCF run 
    	 1  [0.004sec/1.552sec] 
    	 2  [0sec/1.588sec] 
    	 3  [0sec/1.536sec] 
    	 4  [0.004sec/1.556sec] 
    	 5  [0sec/1.528sec] 
    	 6  [0sec/1.652sec] 
    	 7  [0sec/1.58sec] 
    	 8  [0sec/1.632sec] 
    	 9  [0.004sec/1.668sec] 
    	 10  [0sec/1.568sec] 
    UBCF run 
    	 1  [0.004sec/1.548sec] 
    	 2  [0sec/1.572sec] 
    	 3  [0sec/1.576sec] 
    	 4  [0sec/1.556sec] 
    	 5  [0.004sec/1.74sec] 
    	 6  [0.004sec/1.592sec] 
    	 7  [0sec/1.596sec] 
    	 8  [0.004sec/1.664sec] 
    	 9  [0.004sec/1.6sec] 
    	 10  [0sec/1.556sec] 





![png](hw7_files/hw7_20_1.png)




    
